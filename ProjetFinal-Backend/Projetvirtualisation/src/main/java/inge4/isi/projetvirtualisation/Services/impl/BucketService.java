package inge4.isi.projetvirtualisation.Services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.Bucket;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.ListBucketsResponse;
import software.amazon.awssdk.services.s3.model.PutBucketPolicyRequest;

@Service
public class BucketService {


    private final S3Client s3Client;

    public BucketService(S3Client s3Client) {
        this.s3Client = s3Client;
    } 

    public void createBucket(String bucketName) {
        if (!doesBucketExist(bucketName)) {
            CreateBucketRequest request = CreateBucketRequest.builder()
                    .bucket(bucketName)
                    .build();
            s3Client.createBucket(request);
            System.out.println("Bucket " + bucketName + " créé avec succès.");
        } else {
            System.out.println("Le bucket " + bucketName + " existe déjà.");
        }
    }

    private boolean doesBucketExist(String bucketName) {
        ListBucketsResponse response = s3Client.listBuckets();
        for (Bucket bucket : response.buckets()) {
            if (bucket.name().equals(bucketName)) {
                return true;
            }
        }
        return false;
    }
    public void setBucketPolicy(String bucketName) {
        // Définir la politique de bucket en JSON
        String bucketPolicy = "{\n" +
                "    \"Version\": \"2012-10-17\",\n" +
                "    \"Statement\": [\n" +
                "        {\n" +
                "            \"Effect\": \"Allow\",\n" +
                "            \"Principal\": \"*\",\n" +
                "            \"Action\": \"s3:GetObject\",\n" +
                "            \"Resource\": \"arn:aws:s3:::" + bucketName + "/*\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        // Créer une requête pour définir la politique de bucket
        PutBucketPolicyRequest putBucketPolicyRequest = PutBucketPolicyRequest.builder()
                .bucket(bucketName)
                .policy(bucketPolicy)
                .build();

        // Appliquer la politique de bucket
        s3Client.putBucketPolicy(putBucketPolicyRequest);
    }
}


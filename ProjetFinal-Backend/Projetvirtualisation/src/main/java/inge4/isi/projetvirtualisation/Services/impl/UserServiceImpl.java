package inge4.isi.projetvirtualisation.Services.impl;

import inge4.isi.projetvirtualisation.Entities.User;
import inge4.isi.projetvirtualisation.Repositories.UserRepository;
import inge4.isi.projetvirtualisation.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
   private  UserRepository userRepository;


    @Override
    public User createUser(User user) throws IllegalArgumentException {

        Optional<User> utilisateur = userRepository.findByPhone(user.getPhone());
        if (utilisateur.isPresent()){
            throw new IllegalArgumentException("Ce numéro de téléphone est déjà utilisé");
        } else {
            return userRepository.save(user);
        }
    }

        @Override
        public User VerifyUSerById(String email, String mdp) throws IllegalArgumentException {
            Optional<User> utilisateur = userRepository.findById(email);

            if (utilisateur.isPresent()) {
                User user = utilisateur.get();
                if (user.getMdp().equals(mdp)) {
                    System.out.println("L'utilisateur est vérifié");
                    return user;
                }else {
                    System.out.println("Utilisateur non trouvé");
                    throw new IllegalArgumentException("Utilisateur non trouvé");
                }

            }else {
                System.out.println("Utilisateur n'existe pas");
                throw new IllegalArgumentException("Utilisateur non trouvé");
            }
        }

    @Override
    public User MdpRecovery(long phone , String newMdp) throws IllegalArgumentException {
        Optional<User> RegisteredUser = userRepository.findByPhone(phone);
        if (RegisteredUser.isPresent()){
            User user = RegisteredUser.get();
            if (user.getPhone() == phone ){
                System.out.println("Utilisateur reconnu");
                user.setNom(RegisteredUser.get().getNom());
                user.setEmail(RegisteredUser.get().getEmail());
                user.setPrenom(RegisteredUser.get().getPrenom());
                user.setSexe(RegisteredUser.get().getSexe());
                user.setNom_photo(RegisteredUser.get().getNom_photo());
                user.setMdp(newMdp);
               return userRepository.save(user);
            }
        }else {
            System.out.println("Utilisateur non trouvé");
            throw new IllegalArgumentException("Utilisateur non trouvé");
        }
        return null;
    }

    @Override
    public User UpdateNamePhoto(long phone, String nom_photo) throws IllegalArgumentException {
        Optional<User> RegisteredUser = userRepository.findByPhone(phone);
        if (RegisteredUser.isPresent()){
            User user = RegisteredUser.get();
            if (user.getPhone() == phone ){
                user.setNom(RegisteredUser.get().getNom());
                user.setEmail(RegisteredUser.get().getEmail());
                user.setPrenom(RegisteredUser.get().getPrenom());
                user.setSexe(RegisteredUser.get().getSexe());
                user.setMdp(RegisteredUser.get().getMdp());
                user.setNom_photo(nom_photo);
                System.out.println("Utilisateur reconnu et Nom de la photo attribué ! Merci morgane !");
                return userRepository.save(user);
            }
        }else {
            System.out.println("Utilisateur non trouvé");
            throw new IllegalArgumentException("Utilisateur non trouvé");
        }
        return null;
    }
}



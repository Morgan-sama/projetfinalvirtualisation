package inge4.isi.projetvirtualisation.Services;

import inge4.isi.projetvirtualisation.Entities.User;

public interface UserService {

    User createUser(User user)throws IllegalArgumentException;

    User VerifyUSerById(String email, String mdp)throws IllegalArgumentException;

    User MdpRecovery(long phone, String newMdp)throws IllegalArgumentException;

    User UpdateNamePhoto(long phone, String nom_photo)throws IllegalArgumentException;


}



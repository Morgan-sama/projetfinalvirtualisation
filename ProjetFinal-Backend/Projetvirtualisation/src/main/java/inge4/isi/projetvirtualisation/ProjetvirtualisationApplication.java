/* package inge4.isi.projetvirtualisation;

import inge4.isi.projetvirtualisation.Services.impl.BucketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetvirtualisationApplication {

	@Autowired
	public static BucketService bucketService;
	public static void main(String[] args) {
		SpringApplication.run(ProjetvirtualisationApplication.class, args);

	 bucketService.createBucket("Photos");

	}

} */

package inge4.isi.projetvirtualisation;
import inge4.isi.projetvirtualisation.Services.impl.BucketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class ProjetvirtualisationApplication {

	@Autowired
	private BucketService bucketService;

	public static void main(String[] args) {
		SpringApplication.run(ProjetvirtualisationApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void createBucketOnStartup() {
		bucketService.createBucket("photos");
		bucketService.setBucketPolicy("photos");
	}
}


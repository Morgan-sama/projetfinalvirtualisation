package inge4.isi.projetvirtualisation.Config;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3ClientBuilder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URI;

@Configuration
public class S3Config {

    @Bean
    public S3Client s3Client() {
        AwsBasicCredentials awsBasicCredentials = AwsBasicCredentials.create("test", "test");

        S3ClientBuilder clientBuilder = S3Client.builder()
                .endpointOverride(URI.create("http://localhost:4566")) // URL de LocalStack
                .credentialsProvider(() -> awsBasicCredentials)
                .region(Region.US_EAST_1); // Spécification d'une région

        return clientBuilder.build();
    }
}

package inge4.isi.projetvirtualisation.Controllers;

import inge4.isi.projetvirtualisation.Entities.User;
import inge4.isi.projetvirtualisation.Services.UserService;
import inge4.isi.projetvirtualisation.Services.impl.FileService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@RestController
@AllArgsConstructor
@NoArgsConstructor
@RequestMapping("User")
@CrossOrigin("http://localhost:4200")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private FileService fileService;

    @PostMapping("addUser")
    public ResponseEntity<User> createUser(@RequestBody User user){
        User savedUser = userService.createUser(user);
        return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
    }

    @GetMapping("login/{email}/{mdp}")
    public ResponseEntity<User> login(@PathVariable("email") String email, @PathVariable("mdp") String mdp ){
        User userLogged = userService.VerifyUSerById(email, mdp);
        return new ResponseEntity<>(userLogged, HttpStatus.OK);
    }

    @GetMapping("recovery/{phone}/{mdp}")
    public ResponseEntity<User> MdpRecovery(@PathVariable("phone") long phone, @PathVariable("mdp") String newMdp){

        User userReached = userService.MdpRecovery(phone, newMdp);
        return new ResponseEntity<>(userReached, HttpStatus.ACCEPTED);
    }

    @PostMapping("/file")
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {

        String bucketName = "photos";
        String fileName;

        try {
            fileName = fileService.uploadFile(file, bucketName);
        } catch (IOException e) {
            return new ResponseEntity<>("File upload failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("File uploaded: " + fileName, HttpStatus.CREATED);
    }


    @GetMapping("/file/{fileName}")
    public ResponseEntity<InputStreamResource> downloadFile(@PathVariable("fileName") String fileName) {
        String bucketName = "photos";

        try {
            InputStream fileInputStream = fileService.downloadFile(bucketName, fileName);
            InputStreamResource resource = new InputStreamResource(fileInputStream);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentLength(fileService.getFileSize(bucketName, fileName))
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }





    @GetMapping("updatenamefile/{phone}/{namefile}")
    public ResponseEntity<User> UpdateNameFile(@PathVariable("phone") long phone, @PathVariable("namefile") String name){
        User userReached = userService.UpdateNamePhoto(phone, name);
        return new ResponseEntity<>(userReached, HttpStatus.ACCEPTED);
    }

}

package inge4.isi.projetvirtualisation.Services.impl;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetUrlRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.core.sync.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class FileService {

    @Autowired
    private S3Client s3Client;
    private String name ;
    public String uploadFile(MultipartFile file, String bucketName) throws IOException {
        String fileName = file.getOriginalFilename();

        PutObjectRequest request = PutObjectRequest.builder()
                .bucket(bucketName)
                .key(fileName)
                .build();

        s3Client.putObject(request, RequestBody.fromInputStream(file.getInputStream(), file.getSize()));
        System.out.println(fileName);

 //       getObjectUrl(fileName);
        return fileName;
    }

    public URI getObjectUrl(String filename) {
        // Créez une requête pour obtenir l'URL
        GetUrlRequest urlRequest = GetUrlRequest.builder()
                .bucket("photos")
                .key(filename)
                .build();

        // Obtenez l'URL de l'objet
        try {
            System.out.println("Le lien vers votre objet est : " + s3Client.utilities().getUrl(urlRequest).toURI());
            return s3Client.utilities().getUrl(urlRequest).toURI();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public InputStream downloadFile(String bucketName, String fileName) throws IOException {
        // Créer une requête pour obtenir le fichier depuis S3
        GetObjectRequest request = GetObjectRequest.builder()
                .bucket(bucketName)
                .key(fileName)
                .build();

        // Obtenir le flux d'entrée du fichier à partir de S3
        ResponseInputStream<?> s3ObjectStream = s3Client.getObject(request);

        // Définir le dossier cible
        String targetFolder = "C:/ecole/Semestre 2/Virtualisation/Projet/ProjetFinal/src/assets/photos";
        Path targetPath = Paths.get(targetFolder, fileName);

        // Copier le fichier à partir du flux S3 vers le chemin cible
        Files.copy(s3ObjectStream, targetPath);

        // Fermer le flux d'entrée S3
        s3ObjectStream.close();
        return null;
    }

    public long getFileSize(String bucketName, String fileName) {
        HeadObjectRequest request = HeadObjectRequest.builder()
                .bucket(bucketName)
                .key(fileName)
                .build();

        // Obtenir la taille du fichier à partir de S3
        return s3Client.headObject(request).contentLength();
    }


}


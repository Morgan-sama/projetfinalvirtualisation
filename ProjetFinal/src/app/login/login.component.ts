import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../Services/login.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(private router:Router,private user : LoginService, public toastr : ToastrService){}
  goTOregidter(){
    this.router.navigate(['register'])
  }
  goTOreset(){
    this.router.navigate(['reset'])
  }


  showError(){
    this.toastr.error('La connexion a échoué, verifiez l\'email ou le mot de passe ', 'Action échouée', {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'decreasing',
      closeButton: true,
    });
  }


  modelUserToLogin = {
    email: "",
    mdp: "",
  }

  submitFormLogin(modelUserToLogin: any) {
    if (modelUserToLogin.email == ""|| modelUserToLogin.mdp == "") {
      this.showError();
    } else {
       console.log(this.modelUserToLogin);
        this.user.LoginUser(modelUserToLogin.email, modelUserToLogin.mdp).subscribe(
          (data :any) => {
            console.log(data);
          },
          (error : any ) => {
            console.log(error);
          }
        )
      } 
    }
    

  }









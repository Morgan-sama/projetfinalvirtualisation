import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { UserloggedComponent } from './userlogged/userlogged.component';
import { RegisterComponent } from './register/register.component';
import { ResetComponent } from './reset/reset.component';
import { ProfilComponent } from './profil/profil.component';
import { authGuard } from './auth.guard';

const routes: Routes = [

  {path:'login',component:LoginComponent},
  {path:'profile',component: UserloggedComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'reset', component: ResetComponent},
  {path: 'profil', component: ProfilComponent,canActivate: [authGuard]},
  {path:'',redirectTo:'login',pathMatch:'full'},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

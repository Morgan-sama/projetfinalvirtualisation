import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../Services/login.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent {
  constructor(private router:Router,private user : LoginService, public toastr : ToastrService){}

  goTOregidter(){
    this.router.navigate(['register'])
  }


  showError(){
    this.toastr.error('Ce numéro n\'existe pas ou les mots de passe ne sont pas identiques', 'Action échouée', {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'decreasing',
      closeButton: true,
    });
  }


  modelUserToRecovery = {
    phone: 0,
    mdp1: "",
    mdp2: "",
  }

  submitFormReset(modelUserToRecovery: any) {
    if ((modelUserToRecovery.phone == ""|| modelUserToRecovery.mdp == "") && (modelUserToRecovery.mdp1 != modelUserToRecovery.mdp2 )) {
      this.showError();
    } else {
       console.log(modelUserToRecovery);
        this.user.Recoverymdp(modelUserToRecovery.phone, modelUserToRecovery.mdp2).subscribe(
          (data :any) => {
            console.log(data);
          },
          (error : any ) => {
            console.log(error);
          }
        )
      } 
    }


}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class UploadFileServiceService {

  constructor(private http: HttpClient, private toastr : ToastrService) {}

 result :any ;
 result2 : any;

UpdateNameFile(file: File) {
    const formData = new FormData();
    formData.append('file', file);
    var namefile = file.name
    console.log(namefile);
    var temp : any = localStorage.getItem('logged')
    this.result = JSON.parse(temp)
    var phone = this.result.phone;
    console.log(phone);
    return this.http.get('http://localhost:9097/User/updatenamefile/'+phone +'/'+ namefile,{ responseType: 'text' })        
}

Uploadfile (file: File){
  const formData = new FormData();
    formData.append('file', file);
  return this.http.post('http://localhost:9097/User/file', formData, { responseType: 'text' }).pipe(tap(()=>{
    this.showWarning()
  }))
}


  getPhoto(nom: String){

    return this.http.get('http://localhost:9097/User/file/'+ nom)
  }


  showWarning() {
    
    this.toastr.warning('Photo ajoutée avec succès, reconnectez vous pour que les changements soient effectifs', 'Action terminée', {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'decreasing',
      closeButton: true,
    });
  
}



}

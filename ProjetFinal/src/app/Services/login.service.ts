import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  control : any ;

  BASE_URL = 'http://localhost:9097/User/'

  constructor(private http : HttpClient, public toastr: ToastrService, private router:Router) { }
  goToLogin(){
    this.router.navigate(['login'])
  }
  goTOconnect(){
    this.router.navigate(['profil'])
  }

  createUser(user: any) {
    return this.http.post(this.BASE_URL +'addUser', user).pipe(
      tap(() => {
  
        this.showSuccess3();
        
      }),
      catchError((error) => {
        this.showError2();
        console.error('Erreur lors de la création de l\'étudiant :', error);
        return throwError(error);
      })
    );
  }

  LoginUser(mdp: string, email: string):any{
    return this.http.get(this.BASE_URL + 'login/' + email + '/' + mdp).pipe(
        tap((response: any) => {
            if (response) {
                this.control = true; 
                localStorage.setItem('logged', JSON.stringify(response));
                this.showSuccess2();
                this.goTOconnect();
            } else {
                this.control = false;
            }
        }),
        catchError((error) => {
            this.control = false;
            this.showError();
            console.error('Erreur lors de la connexion', error);
            return throwError(error);
        })
    );
}

  Recoverymdp(phone : number , mdp : String){

   return  this.http.get(this.BASE_URL+'recovery/'+ phone + '/'+ mdp).pipe(
      tap(()=>{
        this.showSuccess4();
      }),
      catchError((error) =>{
        this.showWarning()
        console.log('erreur lors de la modifcation',error);
        return (error)
      })
     
    )
  }


  authFunction(): boolean {
    if (this.control) {
        return true;
    } else {
        return false;
    }
}



  showSuccess2() {
    
    this.toastr.success('Utilisateur authentifié avec succès', 'Action terminée', {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'decreasing',
      closeButton: true,
    });
  
}
showSuccess3() {
 
    this.toastr.success('Utilisateur ajouté avec succès', 'Action terminée', {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'decreasing',
      closeButton: true,
    });
  
    this.goToLogin();
}

showSuccess4() {
 
  this.toastr.success('Mot de passe modifié avec succès', 'Action terminée', {
    timeOut: 2000,
    progressBar: true,
    progressAnimation: 'decreasing',
    closeButton: true,
  });

  this.goToLogin();

}
showError() {
  this.toastr.error('La connexion a echoué, verifiez les champs', 'Action échouée', {
    timeOut: 2000,
    progressBar: true,
    progressAnimation: 'decreasing',
    closeButton: true,
  });
}
showError2() {
  this.toastr.error('La création d\'utilisateur a échoué , le numéro de téléphone existe déjà', 'Action échouée', {
    timeOut: 2000,
    progressBar: true,
    progressAnimation: 'decreasing',
    closeButton: true,
  });
}
showWarning() {
  this.toastr.warning('Ce numéro de téléphone n\'est pas reconnu, veuillez réessayer avec un id différent', 'Action échouée', {
    timeOut: 2000,
    progressBar: true,
    progressAnimation: 'decreasing',
    closeButton: true,
  });
}


}

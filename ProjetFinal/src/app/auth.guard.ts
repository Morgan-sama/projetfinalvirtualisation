import { Injectable } from '@angular/core';
import { CanActivateFn } from '@angular/router';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from './Services/login.service';



@Injectable({
  providedIn: 'root'
})
export class authGuard implements CanActivate {
  constructor(private authService: LoginService, private router: Router) {}

  canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

      const isAuthenticated = this.authService.authFunction();
      

      if (isAuthenticated) {
          return true;
      } else {

          this.router.navigate(['/login']);
          return false;
      }
  }
}


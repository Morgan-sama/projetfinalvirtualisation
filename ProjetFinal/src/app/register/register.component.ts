import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../Services/login.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  constructor(private router:Router,private user : LoginService, public toastr : ToastrService ){}

  goTOregidter(){
    this.router.navigate(['login'])
  }

  showError(){
    this.toastr.error('La création a échoué, verifiez les champs', 'Action échouée', {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'decreasing',
      closeButton: true,
    });
  }

  showWarning(){
    this.toastr.warning('Le numéro existe déjà, veuillez réessayer avec un id ou un code différent', 'Action échouée', {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'decreasing',
      closeButton: true,
    });
  }


  modelUserToAdd = {
    email: "",
    mdp: "",
    nom: "",
    prenom: "",
    sexe: "",
    phone: "",
    nom_photo:"",
  }

  submitFormAdd(modelUserToAdd: any) {
    if (modelUserToAdd.sexe == "" || modelUserToAdd.email == "" || modelUserToAdd.prenom == "" || modelUserToAdd.nom == "" || modelUserToAdd.mdp == "" || modelUserToAdd.phone == null ) {
      this.showError();

    } else {
       console.log(modelUserToAdd);
        this.user.createUser(modelUserToAdd).subscribe(
          data => {
            console.log(data);
          },
          error => {
            console.log(error);
          }
        )
      } 
    }

}

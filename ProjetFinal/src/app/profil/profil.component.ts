import { Component, OnInit } from '@angular/core';
import { UploadFileServiceService } from '../Services/upload-file-service.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit{

  constructor(private file: UploadFileServiceService) {}

 temp : any;
 photo : any;
 private isInitialized = false ;
 
ngOnInit(): void {
  
    this.AfficherPhotos()
    
}
  onFileChange(event: any) {
    if (event.target && event.target.files && event.target.files.length > 0) {
        const file = event.target.files[0];

        if (file) {
            var fileName = file.name;
            console.log('Nom du fichier sélectionné:', fileName);        
            this.file.UpdateNameFile(file).subscribe();
            this.file.Uploadfile(file).subscribe();
        }
    } else {
        console.warn('Aucun fichier sélectionné.');
    }
};

AfficherPhotos(){
 
var result : any = localStorage.getItem('logged')
this.temp = JSON.parse(result)
this.file.getPhoto(this.temp.nom_photo).subscribe();
this.photo = this.temp.nom_photo
//  location.reload();
}



}
